# Monitoring Ubuntu servers using Prometheus and Grafana

### Description
Build an automated and efficient monitoring system for servers running the Ubuntu operating system, enabling system administrators to easily monitor and analyze the performance of the servers.

- Monitor the performance and status of Ubuntu server systems.
- Track system resources and detect issues early for timely intervention.
- Analyze trends and predict resource needs to optimize system performance.

### Demo

![Untitled](asset/gra1.png)

CPU usage and server metrics.

![Untitled](asset/gra2.png)

Other metrics.

### Installing Prometheus

Downloads Prometheus at 

[Download | Prometheus](https://prometheus.io/download/)

Here, I'm downloading the version `prometheus-2.45.4.linux-amd64`. Then, I'll proceed to extract it.

```bash
tar -xzvf prometheus-2.45.4.linux-amd64.tar.gz
```

Move folder prometheus to bin:

```bash
mv prometheus-2.45.4.linux-amd64/prometheus /usr/local/bin/
```

In the `prometheus-2.45.4.linux-amd64` folder, there is another useful tool called `promtool`, which is used to check the syntax of `prometheus.yml` configuration files.

```bash
mv prometheus-2.45.4.linux-amd64/promtool /usr/local/bin/
```

Check to see if the installation was successful:

```bash
prometheus --version
prometheus, version 2.39.0 (branch: HEAD, revision: 6d7f26c46ff70286944991f95d791dff03174eea)
  build user:       root@bc053716806f
  build date:       20221005-05:09:43
  go version:       go1.19.1
  platform:         linux/amd64
```

Move the configuration file to the folder **`/etc/prometheus/`**

```bash
mkdir -p /etc/prometheus
mv prometheus-2.45.4.linux-amd64/prometheus.yml /etc/prometheus/
```

At /etc/systemd/system/ create file prometheus.service

```bash
nano /etc/systemd/system/prometheus.service
```

Configuration file:

```yaml
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target
[Service]
ExecStart=/usr/local/bin/prometheus --config.file=/etc/prometheus/prometheus.yml
Restart=always
[Install]
WantedBy=multi-user.target
```

Reload system:

```bash
systemctl daemon-reload
systemctl restart prometheus
systemctl enable prometheus
systemctl status prometheus
```

Check and see if the status is running:

![Untitled](images/Untitled0.png)

Check the status locally for Prometheus by accessing [http://localhost:9090](http://localhost:9090)

![Untitled](images/Untitled1.png)

Check the process of fetching metrics at [http://localhost:9090/metrics](http://localhost:9090/metrics)

![Untitled](images/Untitled2.png)

### Installing node_exporter

Access the [Releases](https://github.com/prometheus/node_exporter/releases) page, find version latest of Node Exporter and downloads.

Extract the files and move them to the `bin` directory.

```bash
tar -xzvf <folder_exporter>
mv <folder_exporter>/node_exporter /usr/local/bin/
```

Check version of Node Exporter:

```bash
node_exporter --version
node_exporter, version 1.5.0 (branch: HEAD, revision: 1b48970ffcf5630534fb00bb0687d73c66d1c959)
  build user:       root@6e7732a7b81b
  build date:       20221129-18:59:09
  go version:       go1.19.3
  platform:         linux/amd64
```

If you can print it as above, it's successfully installed. Next, run the Node Exporter.

```bash
node_exporter

...
level=info msg="Listening on" address=[::]:9100
level=info msg="TLS is disabled." http2=false address=[::]:9100
```

By default, when running the Node Exporter, it operates on port 9100, and the endpoint to fetch metrics is `/metrics`. If you prefer to change the default port and endpoint, you can do so using the `--web.listen-address` and `--web.telemetry-path` flags as follows.

```bash
node_exporter --web.listen-address=":9600" --web.telemetry-path="/node_metrics"
```

Add user node_exporter:

```bash
useradd -rs /bin/false node_exporter
```

Set permissions for the binary file:

```bash
chown node_exporter:node_exporter /usr/local/bin/node_exporter
```

Create file node_exporter.service at /etc/systemd/system/

```bash
[Unit]
Description=Node Exporter
After=network-online.target
[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter
[Install]
WantedBy=multi-user.target
```

Reload node_exporter:

```bash
systemctl daemon-reload
systemctl start node_exporter
systemctl enable node_exporter
systemctl status node_exporter
```
If at this point you see "active (running)" change to green, it indicates success. Reconfigure the `prometheus.yml` file.

In the `scrape_configs` section, we add a `job_name` for Node Exporter.

```yaml
...
scrape_configs:
  - job_name: "prometheus"
    static_configs:
      - targets: ["localhost:9090"]
  - job_name: "node"
    static_configs:
      - targets: ["localhost:9100"]
```

If we install Node Exporter on the server running Prometheus, we specify the `targets` as `["localhost:9100"]`. However, if we need to monitor another server and we install Node Exporter on it, then the `targets` value will be the IP address of the server we want to monitor.

```yaml
...
- targets: ["<remote-server-ip>:9100"]
```

Reload Prometheus.

```bash
systemctl daemon-reload
systemctl restart prometheus
systemctl enable prometheus
systemctl status prometheus
```

Open the browser and navigate to the Prometheus UI. In the search bar, enter the value `{job="node"}`.

![Untitled](images/Untitled4.png)

You'll see a series of metrics collected by Prometheus from the Node Exporter

### Installing Grafana

Access link: 

[Download Grafana | Grafana Labs](https://grafana.com/grafana/download?platform=linux)

Use the following three commands to install, or follow the instructions on the website:

```bash
sudo apt-get install -y adduser libfontconfig1 musl
wget https://dl.grafana.com/enterprise/release/grafana-enterprise_10.4.2_amd64.deb
sudo dpkg -i grafana-enterprise_10.4.2_amd64.deb
```

Proceed with restarting and starting the grafana-server:

```bash
systemctl restart grafana-server
systemctl enable grafana-server
systemctl status grafana-server
```

![Untitled](images/Untitled3.png)

This means Grafana has been successfully launched.

Access Grafana at http://localhost:3000 to log in.

Proceed with importing Dashboards as usual and select Prometheus as the Database.
